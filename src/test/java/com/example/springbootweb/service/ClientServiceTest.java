package com.example.springbootweb.service;

import com.example.springbootweb.model.Client;
import com.example.springbootweb.repository.ClientRepository;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class ClientServiceTest {

    private static final EasyRandom RANDOM = new EasyRandom();

    @Mock
    private ClientRepository clientRepository;

    @InjectMocks
    private ClientService clientService;

    @Test
    public void getClient_whenExists_shouldReturn() {
        //Given
        Client client = RANDOM.nextObject(Client.class);
        Mockito.when(clientRepository.findById(client.getId())).thenReturn(Optional.of(client));

        //When
        Optional<Client> result = clientService.getClient(client.getId());

        //Then
        assertTrue(result.isPresent());
        assertEquals(result.get(), client);
    }

    @Test
    public void getClient_whenNotExists_shouldReturnEmpty() {
        //Given
        Mockito.when(clientRepository.findById(any())).thenReturn(Optional.empty());

        //When
        Optional<Client> result = clientService.getClient(UUID.randomUUID());

        //Then
        assertTrue(result.isEmpty());
    }

    @Test
    public void getClient_whenNull_shouldThrow() {
        //Given

        //When
        Executable executable = () -> clientService.getClient(null);

        //Then
        assertThrows(NullPointerException.class, executable);
    }

}
