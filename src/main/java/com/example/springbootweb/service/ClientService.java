package com.example.springbootweb.service;

import com.example.springbootweb.model.Client;
import com.example.springbootweb.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ClientService {

    private final ClientRepository clientRepository;

    public Optional<Client> getClient(UUID id) {
        if (id == null) throw new NullPointerException("ID is null");

        return clientRepository.findById(id);
    }
}
